# Exerecises

## Tested site

https://vndb.org/

At the time of starting the exercise, nobody has used this website for task:

![nobody used](resources/noone_used.png)

## Testing workflows

### Registration process

| No | Action/Expected result | Status | Comment |
|---|---|---|---|
| 1 | Open vndb.org in firefox browser | Correct | Opens main page `/` |
| 2 | Press 'register' button | Correct | Opens '/u/register' page |
| 3 | Type correct email, anti-bot answer, and check required checkboxes, to be restricted only by name | Correct |  |
| 4 | Type username that is db identifier (u1618123), press 'Submit' | Correct | Rejects name |
| 5 | Type username that looks like db identifier, but with 3 replaced by cyrillic capital 'з' (u1618123), press 'Submit' | Correct | Rejects name |
| 6 | Type regular username containing cyrillic letters (амогусsuslekele), press 'Submit' | Correct | Rejects name |
| 7 | Type single-character name (a), press 'Submit' | Correct | Rejects name |
| 8 | Type 16 characters (123456789tettffs) | Correct | HTML property didn't allow to write 16th |
| 9 | Change 'maxlength' to 16, type 16 characters (123456789tettffs), press 'Submit' | Correct | Displays 'Invalid form data, please report a bug.' |
| 10 | Type name with special characters (aboba!!!) | Correct | Rejects name |
| 11 | Type occupied username (admin) | Correct | Reports that username is occupied |
| 12 | Write valid and free username (aboba123456) | Correct | Displays "email sent" page, email is received at chosen address. |
| 13 | Open register page and type all correct fields | Correct | Empty register page is opened, fields are filled successfully |
| 14 | Write occupied email, press 'Submit' | Correct | Rejects registration |
| 15 | Write email without top-level domain (aboba@aaads) | Correct | Rejects email |
| 16 | Write email without at sign (abobaa.aads) | Correct | Rejects email |
| 17 | Write correct email again (aboba@a.aads) | Correct | Rejects second registration from the same IP within 24 hours |
| 18 | Write incorrect answer to anti-bot question (42448, correct is 42449), should write that it was answered incorrectly | BUG | Allows registration |
| 19 | Write more incorrect answer to anti-bot question (42236, correct is 42449) | Correct | Rejects the answer |
| 20 | Write correct answer back, uncheck 18 y.o. box | Correct | 'Submit' button is hidden |
| 21 | Check it back, uncheck privacy policy box | Correct | 'Submit' button is hidden |
| 22 | Follow link from email | Correct | Page for setting password is opened |
| 23 | Write incorrect passwords (1, a), press 'set password' | Correct | Rejects too weak password |
| 24 | Write quite long password containing random characters from keyboard (`@!#$%T~~Z±§§±§!@#$%^&*()_\|":>BL"H<N":Z`), press the button | Correct | Accepts the password |

### Search

| No | Action/Expected result | Status | Comment |
|---|---|---|---|
| 1 | Open main page | Correct | Opens `/` page |
| 2 | Type 'Higurashi' in search window, press 'Enter' on keyboard | Correct | Opens search page with 'Higurashi no Naku Koro ni' in results list |
| 3 | Type 'Higurashi     ' (with spaces), press 'Search' | Correct | Opens search page with 'Higurashi no Naku Koro ni' in results list |
| 4 | Type 'hIgUrAsHi', press 'Search' | Correct | Opens search page with 'Higurashi no Naku Koro ni' in results list |


### Tags hiding

| No | Action/Expected result | Status | Comment |
|---|---|---|---|
| 1 | Open page https://vndb.org/v67 | Correct | Page with novel 'Higurashi no Naku Koro ni' is opened, list of tags is visible, there are no spoiler tags, as well as buttons 'hide spoilers', 'show minor spoilers', 'spoil me!' |
| 2 | Press button 'show minor spoilers' | Correct | Tags with minor spoilers appear. Among them are 'Curse' and 'Homicide'. |
| 3 | Press button 'spoil me!' | Correct | Tags with major spoilers appear. Among them are 'Yangire Heroine' and 'Madness'. |
| 4 | Press button 'hide spoilers' | Correct | All spoiler tags are hidden. |

## Automation

I chose the last workflow "tags hiding".
The automated test is located in `main.py`
