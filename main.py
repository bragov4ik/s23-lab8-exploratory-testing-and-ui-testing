from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By


def get_visible_tags(driver):
    tags_element = driver.find_element(By.ID, 'vntags')
    return list(map(
        lambda e: e.text, filter(
            lambda e: e.is_displayed(),
            tags_element.find_elements(By.TAG_NAME, 'a')
        )
    ))


def test_tags():
    # Initialize
    driver = webdriver.Remote(
        command_executor="http://selenium__standalone-chrome:4444/wd/hub",
        options=Options()
    )

    # Open site
    driver.get('https://vndb.org/v67')

    # Check that the novel is correct
    assert 'Higurashi no Naku Koro ni' in driver.page_source

    normal_tags = ["Friendship", "Psychological Horror"]

    minor_spoilers = ["Curse", "Homicide"]

    major_spoilers = ["Yangire Heroine", "Madness"]

    # Check that all spoilers are hidden, common are present

    all_tags = get_visible_tags(driver)
    for normal in normal_tags:
        assert normal in all_tags
    for minor in minor_spoilers:
        assert minor not in all_tags
    for major in major_spoilers:
        assert major not in all_tags

    # Press button to reveal some spoilers

    driver.find_element(By.CSS_SELECTOR, "label[for='tag_spoil_some']").click()

    # They should appear in the list, major still invisible

    all_tags = get_visible_tags(driver)

    for normal in normal_tags:
        assert normal in all_tags
    for minor in minor_spoilers:
        assert minor in all_tags
    for major in major_spoilers:
        assert major not in all_tags

    # Press button to reveal all spoilers

    driver.find_element(By.CSS_SELECTOR, "label[for='tag_spoil_all']").click()

    # All tags are visible

    all_tags = get_visible_tags(driver)
    for normal in normal_tags:
        assert normal in all_tags
    for minor in minor_spoilers:
        assert minor in all_tags
    for major in major_spoilers:
        assert major in all_tags

    # Hide all spoilers

    driver.find_element(By.CSS_SELECTOR, "label[for='tag_spoil_none']").click()

    # Check that all spoilers are hidden, common are present

    all_tags = get_visible_tags(driver)
    for normal in normal_tags:
        assert normal in all_tags
    for minor in minor_spoilers:
        assert minor not in all_tags
    for major in major_spoilers:
        assert major not in all_tags

    # Close
    driver.quit()


test_tags()
